import 'dart:io';
import './dummy_data.dart';
import 'models/menu.dart';

int selectedCatNumber = -1;
int selectedMenuNumber = -1;
void main(List<String> arguments) {
  bool menuLoop = true;
  num orderPrice = 0;

  while (menuLoop == true) {
    showCategories();
    int catNumber = int.parse(stdin.readLineSync()!);
    selectedCatNumber = catNumber;
    selectCategories(catNumber);
    int menuNumber = int.parse(stdin.readLineSync()!);
    selectedMenuNumber = menuNumber;
    if (menuNumber != 0) {
      menuLoop = false;
    }
  }
  showOption(selectedMenuNumber, selectedCatNumber);
  int option = int.parse(stdin.readLineSync()!);
  orderPrice = selectOption(option, selectedMenuNumber, selectedCatNumber);
  showSweetness();
  int sweetness = int.parse(stdin.readLineSync()!);
  print('กรุณาชำระเงิน $orderPrice บาท');
  showPaymentOption();
  int paymentOption = int.parse(stdin.readLineSync()!);
  print('ชำระเงินสำเร็จ');
  summarize(selectedCatNumber);
}

void summarize(int selectedCatnumber) {
  if (selectedCatnumber != 1) {
    for (var element in dummyMenus) {
      if (element.getCategory[0] == selectedCatNumber &&
          element.getCategoryNumber[0] == selectedMenuNumber) {
        print('${element.title} ของท่านจะพร้อมใน ${element.duration} วินาที');
      }
    }
  } else {
    for (var element in dummyMenus) {
      if (element.category.length > 1) {
        if (element.getCategory[1] == selectedCatNumber &&
            element.getCategoryNumber[1] == selectedMenuNumber) {
          print('${element.title} ของท่านจะพร้อมใน ${element.duration} วินาที');
        }
      }
    }
  }
}

void showPaymentOption() {
  print('โปรดใส่หมายเลขเพื่อช่องทางชำระเงิน');
  print('1 เงินสด');
  print('2 สแกนQR');
  print('3 เต่าบินเครดิต');
  print('4 ใช้/ดูคูปอง');
  print('5 E-Wallet');
  print('6 โปรโมชั่นอื่นๆ');
}

void showSweetness() {
  print('โปรดใส่หมายเลขเพื่อเลือกระดับความหวาน');
  print('1 ไม่ใส่น้ำตาล');
  print('2 หวานน้อย');
  print('3 หวานพอดี');
  print('4 หวานมาก');
  print('5 หวาน3โลก');
}

num selectOption(int option, int selectedMenuNumber, int selectedCatNumber) {
  if (selectedCatNumber != 1) {
    for (var element in dummyMenus) {
      if (element.getCategory[0] == selectedCatNumber &&
          element.getCategoryNumber[0] == selectedMenuNumber) {
        if (option == 1) {
          return element.price;
        }
        if (option == 2) {
          return element.price + 5;
        }
        if (option == 3) {
          return element.price + 10;
        }
      }
    }
  } else {
    for (var element in dummyMenus) {
      if (element.category.length > 1) {
        if (element.getCategory[1] == selectedCatNumber &&
            element.getCategoryNumber[1] == selectedMenuNumber) {
          if (option == 1) {
            return element.price;
          }
          if (option == 2) {
            return element.price + 5;
          }
          if (option == 3) {
            return element.price + 10;
          }
        }
      }
    }
  }
  return 0;
}

void showOption(int menuNumber, int catNumber) {
  print('โปรดใส่หมายเลขเพื่อเลือกประเภทเครื่องดื่ม');
  if (catNumber != 1) {
    for (var element in dummyMenus) {
      if (element.getCategory[0] == catNumber &&
          element.getCategoryNumber[0] == menuNumber) {
        if (element.getHot) {
          print('1 ร้อน ${element.getPrice} บาท');
        }
        if (element.getIce) {
          print('2 เย็น ${element.getPrice + 5} บาท');
        }
        if (element.getBlend) {
          print('3 ปั่น ${element.getPrice + 10} บาท');
        }
      }
    }
  } else {
    for (var element in dummyMenus) {
      if (element.category.length > 1) {
        if (element.getCategory[1] == catNumber &&
            element.getCategoryNumber[1] == menuNumber) {
          if (element.getHot) {
            print('1 ร้อน ${element.getPrice} บาท');
          }
          if (element.getIce) {
            print('2 เย็น ${element.getPrice + 5} บาท');
          }
          if (element.getBlend) {
            print('3 ปั่น ${element.getPrice + 10} บาท');
          }
        }
      }
    }
  }
}

void selectCategories(int catNumber) {
  print('โปรดใส่หมายเลขเพื่อเลือกเครื่องดื่ม');
  if (catNumber != 1) {
    for (var element in dummyMenus) {
      if (element.getCategory[0] == catNumber) {
        print('${element.getCategoryNumber[0]} ${element.getTitle}');
      }
    }
  } else {
    for (var element in dummyMenus) {
      if (element.category.length > 1) {
        if (element.getCategory[1] == catNumber) {
          print('${element.getCategoryNumber[1]} ${element.getTitle}');
        }
      }
    }
  }

  print('0 ย้อนกลับ');
}

void showCategories() {
  print('โปรดใส่หมายเลขเพื่อเลือกประเภทเครื่องดื่ม');
  for (var element in dummyCategories) {
    print('${element.getId} ${element.getTitle}');
  }
}

class Category {
  final int id;
  final String title;

  const Category({required this.id, required this.title});

  int get getId {
    return id;
  }

  String get getTitle {
    return title;
  }
}

class Menu {
  final int id;
  final String title;
  final List<int> category;
  final List<int> categoryNumber;
  final int price;
  final int duration;
  final bool hot;
  final bool ice;
  final bool blend;

  const Menu(
      {required this.id,
      required this.title,
      required this.category,
      required this.categoryNumber,
      required this.price,
      required this.duration,
      required this.hot,
      required this.ice,
      required this.blend});

  get getId => this.id;

  get getTitle => this.title;

  get getCategory => this.category;

  get getCategoryNumber => this.categoryNumber;

  get getPrice => this.price;

  get getDuration => this.duration;

  get getHot => this.hot;

  get getIce => this.ice;

  get getBlend => this.blend;
}
